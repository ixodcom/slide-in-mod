Slide Pages
============

Allows admins to add Slide Pages (html or markdown) or external links to various navigations (e.g. top navigation, account menu).

__Status:__ alpha
__Module website:__ <https://github.com/humhub/humhub-modules-custom-pages>
__Author:__ Dinesh Laller
__Author website:__ [humhub.org](http://humhub.org)


For more  informations visit:
<https://github.com/humhub/humhub-modules-custom-pages>
