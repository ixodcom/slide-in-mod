<?php

/**
 * HumHub
 * Copyright © 2014 The HumHub Project
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 */

/**
 * Description of PageSlidersEvents
 *
 * @author luke
 */
class PageSlidersEvents
{
    public static function onAdminMenuInit($event)
    {
		Yii::app()->clientScript->registerScriptFile(
		Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.modules.page_sliders.assets.js') . '/custom_sliders.js'));

		Yii::app()->clientScript->registerCssFile(
		Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.modules.page_sliders.assets.css') . '/custome_pages.css'));

        $event->sender->addItem(array(
            'label' => Yii::t('PageSlidersModule.base', 'Slide Pages'),
            'url' => Yii::app()->createUrl('//page_sliders/admin'),
            'group' => 'manage',
            'icon' => '<i class="fa fa-file-o"></i>',
            'isActive' => (Yii::app()->controller->module && Yii::app()->controller->module->id == 'page_sliders' && Yii::app()->controller->id == 'admin'),
            'sortOrder' => 300,
        ));

        // Check for Admin Menu Pages to insert
    }

    public static function onTopMenuInit($event)
    {

		Yii::app()->clientScript->registerScriptFile(
		Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.modules.page_sliders.assets.js') . '/custom_sliders.js'));

		Yii::app()->clientScript->registerCssFile(
		Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.modules.page_sliders.assets.css') . '/custome_pages.css'));

        foreach (PageSlider::model()->findAll() as $page) {

            // Admin only
            if ($page->admin_only == 1 && !Yii::app()->user->isAdmin()) {
                continue;
            }
			//print_r($event->sender);
            $event->sender->addItem(array(
                'label' => $page->title,
				'id' => 'slidePageSliders',
                'url' => Yii::app()->createUrl('//page_sliders/view', array('id' => $page->id)).'|'.$page->position,
				'target' => ($page->type == PageSlider::TYPE_LINK) ? '_blank' : '',
                'icon' => '<i class="fa ' . $page->icon . '"></i>',
                'isActive' => (Yii::app()->controller->module && Yii::app()->controller->module->id == 'page_sliders' && Yii::app()->controller->id == 'view' && Yii::app()->request->getParam('id') == $page->id),
                'sortOrder' => ($page->sort_order != '') ? $page->sort_order : 1000,
            ));
        }
		
   
    }
}
