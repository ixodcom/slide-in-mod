$(document).ready(function()
{

	$('body').on('click', '.hide_slide', function()
	{
		$('.custom_page_content').css({
			'top'		: '0px',
			'bottom'	: '0px',
			'width'		: '0px',
			'height'	: '0px',
			'padding'	: '0px'
			}).hide();
	});
	
	$('body').on('click', '.reload_iframe', function()
	{
	
		var class_get = $(this).attr('alt');
		$('.'+class_get).attr('src', $('.'+class_get).attr('src'));
		
	});

	$('.slidePageSliders').click(function(e)
	{
		e.preventDefault();
		var href_link	 = ($(this).find('a').attr('href')).split('|');
		
		$('.visible-md').removeClass('active');
		$(this).addClass('active');
		var div_id 		 = (href_link[0]).replace(/[\/?=.&]/g,'-');

		var div_content  = '<div class="custom_page_content" id="'+div_id+'" style=" ';
			div_content += 'position: relative;top: 0px;background: rgb(252, 252, 252);color:black;z-index:99;">';
			div_content += '<div class="slide_title" style="background: #fff;padding: 8px;color: #1BAD15;font-size: 14px;">';
			div_content += '<span style="vertical-align: top;">'+$(this).text()+'</span>';     
			div_content += '<div class="slide_cross" style="display: inline-block; float: right; vertical-align: top;width:25px">';
			div_content += '<a href="javascript:void(0);" class="hide_slide">X</a></div>';
			div_content += '<div style="clear:both; line-height:1px; height:1px;"></div></div>';
			div_content += '</div>';

		$('#'+div_id).stop().clearQueue();

		if( $('#'+div_id).length == 0 )
		{			
			$.get( href_link[0], function(content ) 
			{
			
				$('body').append(div_content);
				$('#'+div_id).append('<div style="padding:8px; height:100%; overflow-x:hidden; overflow-y:auto;">'+content+'</div>');
				animate_custom_div(div_id,href_link[1]);	
			});
		}
		else
		{
			animate_custom_div( div_id, href_link[1] );
		}
	});

});

/**
* FUNCTION TO ANIMATE THE CUSTOM PAGE
*
* param div_id string
* param position_type int
*
*/
function animate_custom_div( div_id, position_type )
{

		$('.custom_page_content').css({
			'top'		: '0px',
			'bottom'	: '0px',
			'width'		: '0px',
			'height'	: '0px',
			'padding'	: '0px',
			'overflow'  : 'hidden',
		}).hide();

		var window_width	= $(document).width();
		var window_height	= $(document).height();
		var top_height		= (window_height/2);
		var left_width		= (window_width/2);
		var menu_height 	= $('#topbar-first').outerHeight()+$('#topbar-second').outerHeight();
		var slide_width 	= '640';

		$('#'+div_id).css({'padding': '105px 0px'}).show();

		switch(position_type)
		{
			case '1':	//left
			
				$('#'+div_id).css({
					'position'	: 'absolute',
					'top'   	: '0px',
					'left'  	: '-'+slide_width+'px',
					'width' 	: (slide_width)+'px',
					'height'	: window_height+'px'
				}); 
				
				$('#'+div_id).animate({'left':'0px'});	
				
				break;

			case '2':	//right
			
				$('#'+div_id).css({
					'position'	: 'absolute',
					'top'   	: '0px',
					'right'  	: -(slide_width)+'px',
					'width' 	: (slide_width)+'px',
					'height'	: window_height+'px'					
				}); 
				
				$('#'+div_id).animate({'right':'0px'});	
				
				break;

			case '3':	//bottom
			
				$('#'+div_id).css({
					'position'	: 'absolute',
					'top'		: window_height+'px',
					'left'		: '0px',
					'height'	: (window_height)+'px',
					'width'		: slide_width+'px'
				});		
				
				$('#'+div_id).animate({'top':'0px'});
				
				break;

			case '4':	//top
			
				$('#'+div_id).css({
					'position'	: 'absolute',
					'top'		: '-'+(window_height)+'px',
					'height'	: (window_height)+'px',
					'width'		: slide_width+'px'
				});		
				
				$('#'+div_id).animate({'top':'0px'});
				
				break;

			default:	//top
			
				$('#'+div_id).css({'top':'-'+window_height+'px'});		
				$('#'+div_id).animate({'top':'-'+top_height+'px'});
				
				break;
		}
}