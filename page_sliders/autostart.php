<?php
Yii::app()->moduleManager->register(array(
    'id' => 'page_sliders',
    'class' => 'application.modules.page_sliders.PageSlidersModule',
    'import' => array(
        'application.modules.page_sliders.*',
        'application.modules.page_sliders.models.*',
		'application.modules.page_sliders.assets.*',
		
    ),
    // Events to Catch 
    'events' => array(
        array('class' => 'AdminMenuWidget', 'event' => 'onInit', 'callback' => array('PageSlidersEvents', 'onAdminMenuInit')),
        array('class' => 'TopMenuWidget', 'event' => 'onInit', 'callback' => array('PageSlidersEvents', 'onTopMenuInit'))       
    ),
));
?>