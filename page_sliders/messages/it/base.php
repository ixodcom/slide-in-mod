<?php
return array (
  'Create new Page' => 'Crea nuova pagina',
  'Slide Pages' => 'Pagine personalizzate',
  'HTML' => 'HTML',
  'IFrame' => 'IFrame',
  'Link' => 'Link',
  'MarkDown' => 'MarkDown',
  'Navigation' => 'Menu',
  'No Slide Pages created yet!' => 'Nessun pagina ancora creata!',
  'Sort Order' => 'Ordinamento',
  'Title' => 'Titolo',
  'Top Navigation' => 'Top Menu',
  'Type' => 'Tipo',
  'User Account Menu (Settings)' => 'Menu impostazione per utenti',
);
