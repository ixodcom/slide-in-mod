<?php
return array (
  'Create new Page' => '',
  'Slide Pages' => 'Páginas personalizadas',
  'HTML' => 'HTML',
  'IFrame' => 'IFrame',
  'Link' => 'Link',
  'MarkDown' => 'MarkDown',
  'Navigation' => 'Navegación',
  'No Slide Pages created yet!' => '¡No se han creado páginas personalizadas todavía!',
  'Sort Order' => '',
  'Title' => 'Título',
  'Top Navigation' => '',
  'Type' => 'Tipo',
  'User Account Menu (Settings)' => '',
);
