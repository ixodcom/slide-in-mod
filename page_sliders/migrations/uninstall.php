<?php

class uninstall extends ZDbMigration
{

    public function up()
    {

        $this->dropTable('page_sliders_page');
    }

    public function down()
    {
        echo "uninstall does not support migration down.\n";
        return false;
    }

}
