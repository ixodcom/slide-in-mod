<?php

/**
 * This is the model class for table "page_sliders_page".
 *
 * The followings are the available columns in table 'page_sliders_page':
 * @property integer $id
 * @property integer $type
 * @property string $title
 * @property string $icon
 * @property string $content
 * @property integer $sort_order
 * @property integer $admin_only
 * @property string $navigation_class
 * @position integer $position
 */
class PageSlider extends HActiveRecord
{

    public $url;

    const TYPE_LINK = '1';
    const TYPE_HTML = '2';
    const TYPE_IFRAME = '3';
    const TYPE_MARKDOWN = '4';

	const POSITION_LEFT = '1';
    const POSITION_RIGHT = '2';
    const POSITION_UP = '3';
    const POSITION_DOWN = '4';

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return PageSlider the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'page_sliders_page';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('type,position, title', 'required'),
            array('type,position, sort_order, admin_only', 'numerical', 'integerOnly' => true),
            array('title', 'length', 'max' => 255),
            array('icon', 'length', 'max' => 100),
            array('content, url', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'type' => 'Type',
			'position' => 'Slide Positions',
            'title' => 'Title',
            'icon' => 'Icon',
            'content' => 'Content',
            'url' => 'URL',
            'sort_order' => 'Sort Order',
            'admin_only' => 'Only visible for admins',
            'navigation_class' => 'Navigation',
        );
    }

    public function beforeSave()
    {
        if ($this->type == self::TYPE_IFRAME || $this->type == self::TYPE_LINK) {
            $this->content = $this->url;
        }

        return parent::beforeSave();
    }

    public function afterFind()
    {
        if ($this->type == self::TYPE_IFRAME || $this->type == self::TYPE_LINK) {
            $this->url = $this->content;
        }

        return parent::afterFind();
    }

    public static function getPageTypes()
    {
        return array(
            self::TYPE_HTML => Yii::t('PageSlidersModule.base', 'HTML'),
            self::TYPE_MARKDOWN => Yii::t('PageSlidersModule.base', 'MarkDown'),
            self::TYPE_IFRAME => Yii::t('PageSlidersModule.base', 'IFrame'),
        );
    }

	public static function getPositionTypes()
    {
        return array(
            self::POSITION_LEFT => Yii::t('PageSlidersModule.base', 'Left Slide IN'),
            self::POSITION_RIGHT => Yii::t('PageSlidersModule.base', 'Right Slide IN'),
            self::POSITION_UP => Yii::t('PageSlidersModule.base', 'Bottom Slide Up'),
            self::POSITION_DOWN => Yii::t('PageSlidersModule.base', 'Top Slide Down'),
        );
    }

}
