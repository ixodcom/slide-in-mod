<?php
/**
@$class_name parameter return a random string
*/
function generateRandomString($length = 10) 
{
    $characters 		= '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength 	= strlen($characters);
    $randomString 		= '';

    for ($i = 0; $i < $length; $i++) 
	{
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
    return $randomString;
}

	$class_name 	= generateRandomString(); 
	// save url to standard loader graphic
	$loader_url 	= Yii::app()->baseUrl . "/img/loader.gif";
	$reload_url 	= Yii::app()->controller->module->registerImage('reload.png');

	// check if there is a themed loader graphic and replace the url

	if (file_exists(Yii::getPathOfAlias('webroot') . "/themes/" . Yii::app()->theme->name . "/img/loader.gif")) 
	{
	  $loader_url 	= Yii::app()->theme->baseUrl . "/img/loader.gif";
	}
?>

<?php if($navigationClass == CustomPage::NAV_CLASS_ACCOUNTNAV): ?>
    <iframe class="<?php echo $class_name; ?>" id="iframepage" style="width:100%; height: 100%; margin-top:45px;" src="<?php echo $url; ?>"></iframe>
	<style>
        #iframepage 
		{
            border: none;
            background: url('<?php echo $loader_url; ?>') center center no-repeat;
        }
    </style>
	
	<script>
        window.onload = function (evt) {
            setSize();
        }
        window.onresize = function (evt) {
            setSize();
        }
        function setSize() {

            $('#iframepage').css('height', window.innerHeight - 170 + 'px');
        }
    </script>

<?php else: ?>

<iframe class="<?php echo $class_name; ?>" id="iframepage" style="width:100%;height:100%;margin-top:45px;" src="<?php echo $url; ?>"></iframe>

	<style>
        #iframepage {
            position: absolute;
            left: 0;
            top: 98px; 
			border:1px solid #d4d4d4;
			box-sizing:border-box;
			-moz-box-sizing:border-box;
			-webkit-box-sizing:border-box;
            background: url('<?php echo $loader_url; ?>') center center no-repeat;
        }

    </style>

    <script>

		$(document).ready(function()
		{
		
			var class_name = "<?php echo $class_name;?>";
			
			var append  = '<div class="slide_cross" style="display: inline-block;float: right; vertical-align: top;">';
append += '<a href="javascript:void(0);" class="reload_iframe" alt="'+class_name+'"><img src="<?php echo $reload_url;?>"></a>';
			append += '</div>';
			$('.'+class_name).parents().eq(1).find('.slide_cross').after(append);
		});

    </script>

<?php 
endif; 
die;
?>
