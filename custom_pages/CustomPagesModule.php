<?php
class CustomPagesModule extends HWebModule
{
 public $subLayout = "application.modules_core.user.views.account._layout";
	
 private $_assetsUrl;


	public function getConfigUrl()
    {
        return Yii::app()->createUrl('//custom_pages/admin');
    }

    public function disable()
    {
        if (parent::disable()) {

            foreach (CustomPage::model()->findAll() as $entry) {
                $entry->delete();
            }

            return true;
        }

        return false;
    }

/**
* @register a js and css file.
*/

	public function init()
	{
		Yii::app()->clientScript->registerScriptFile(
		Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.modules.custom_pages.assets.js') . '/custom_sliders.js'));

		Yii::app()->clientScript->registerCssFile(
		Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.modules.custom_pages.assets.css') . '/custome_pages.css'));
	}

/**
* @return string the base URL that contains all published asset files of this module.
*/
public function getAssetsUrl()
{
    if($this->_assetsUrl===null)
       $this->_assetsUrl=Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('custom_pages.assets'));
       return $this->_assetsUrl;
}

/**
 * @param string the base URL that contains all published asset files of this module.
 */
public function setAssetsUrl($value)
{
    $this->_assetsUrl=$value;
}

public function registerImage($file)
{
    return $this->getAssetsUrl().'/img/'.$file;
}
 
}
